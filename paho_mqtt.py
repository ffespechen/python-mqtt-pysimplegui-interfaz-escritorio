#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 22 17:53:35 2020

@author: ffespechen
"""
import PySimpleGUI as sg
import paho.mqtt.client as mqtt

sg.theme('SandyBeach')

def on_connect(client, userdata, rc):
    print('Conectado con código resultante -> ', str(rc))
    client.suscribe('abRxXaQveghM4cz')

def main():
    
    client = mqtt.Client()
    client.username_pw_set(username='dcmLiiy3qMyNUNP', password='2uetpt7lKVYwEYE')
    client.on_connect = on_connect
    
    client.connect('ioticos.org', 1883, 120)
        
    size = (12, 1)
    
    layout = [
            [sg.Column([[sg.Button('IZQ. ARRIBA', size=size, key='I')], 
                         [sg.Button('IZQUIERDA', size=size, key='A')], 
                         [sg.Button('IZQ. ABAJO', size=size, key='J')]]),
             sg.Column([[sg.Button('ADELANTE', size=size, key='W')], 
                         [sg.Button('DETENER', size=size, key='Z')], 
                         [sg.Button('ATRÁS', size=size, key='S')]]),
             sg.Column([[sg.Button('DER. ARRIBA', size=size, key='O')], 
                         [sg.Button('DERECHA', size=size, key='D')], 
                         [sg.Button('DER. ABAJO', size=size, key='L')]])],
            [sg.Text('Estado -> ', size=size, key='-ESTADO-')]
            ]
    
    window = sg.Window('PYTHON MQTT CLIENT',
                       layout,
                       return_keyboard_events=True                  
                       )
    
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED: # if user closes window or clicks cancel
            break
        else:
            client.publish('abRxXaQveghM4cz', event)
            msje = 'Enviado -> ' + str(event)
            window['-ESTADO-'].update(msje)
            
            

    client.disconnect()
    window.close()


if __name__=='__main__':
    main()



